﻿using Discord;
using Discord.WebSocket;
public class Program
{
	List<Board> _boards = new List<Board>();
	string Token = "MTAyMzA5ODE1NzUxMDQzODk1NQ.G79MAj.yvz8soXFfkA6Ex1CdXldBBPUzjvyfUZVsWTIVE";
	ulong ClientId = 1023098157510438955;
	ulong DevServerId = 1003234517697429575;
	private DiscordSocketClient? _client;
	private SocketGuild? _guild;
	public static Task Main(string[] args) => new Program().MainAsync();
	public async Task MainAsync()
	{
		_client = new DiscordSocketClient();
		_client.Log += Log;
		_client.Ready += CreateCommands;
		_client.SlashCommandExecuted += HandleCommand;
		_client.GuildAvailable += AddGuildToBoards;
		await _client.LoginAsync(TokenType.Bot, Token);
		await _client.StartAsync();
		await Task.Delay(-1);
	}
	private Task Log(LogMessage msg)
	{
		Console.WriteLine(msg.ToString());
		return Task.CompletedTask;
	}
	public async Task CreateCommands()
	{
		var commandAdd = new SlashCommandBuilder()
			.WithName("register")
			.WithDescription("Add yourself to the Rocket Board.");
		var commandShow = new SlashCommandBuilder()
			.WithName("show")
			.WithDescription("Shows the Rocket Board.")
			.AddOption("playlist", ApplicationCommandOptionType.String, "Choose: average, best, solo, double, standard, casual", false);
		var commandPlaylist = new SlashCommandBuilder()
			.WithName("playlist")
			.WithDescription("Sets the default playlist of the Rocket Board.")
			.AddOption("playlist", ApplicationCommandOptionType.String, "Choose: average, best, solo, double, standard, casual", true);
		var commandCycle = new SlashCommandBuilder()
			.WithName("cycle")
			.WithDescription("Auto-publish cycle.")
			.AddOption("cycle", ApplicationCommandOptionType.String, "Choose: hourly, daily, weekly, monthly");
		try
		{
			_guild = _client.GetGuild(DevServerId);
			await _guild.DeleteApplicationCommandsAsync();

			await _guild.CreateApplicationCommandAsync(commandAdd.Build());
			await _guild.CreateApplicationCommandAsync(commandShow.Build());
			await _guild.CreateApplicationCommandAsync(commandPlaylist.Build());
			await _guild.CreateApplicationCommandAsync(commandCycle.Build());

			/* await _client.CreateGlobalApplicationCommandAsync(commandAdd.Build());
			await _client.CreateGlobalApplicationCommandAsync(commandMatch.Build());
			await _client.CreateGlobalApplicationCommandAsync(commandReset.Build());
			await _client.CreateGlobalApplicationCommandAsync(commandRestart.Build());
			await _client.CreateGlobalApplicationCommandAsync(commandShow.Build()); */
		}
		catch (Discord.Commands.CommandException exception)
		{
			Console.WriteLine(exception.Message);
		}
	}
	public async Task HandleCommand(SocketSlashCommand command)
	{
		switch (command.Data.Name)
		{
			case "register":
				await AddPlayer(command);
				break;
			case "show":
				await ShowBoard(command);
				break;
			case "playlist":
				await SetPlaylist(command);
				break;
			case "cycle":
				await SetCycle(command);
				break;
			default:
				await command.RespondAsync($"You executed {command.Data.Name}, but nothing happened! You can contact the dev, but he's an idiot.");
				break;
		}
	}
	public async Task AddPlayer(SocketSlashCommand command)
	{
		/* ulong? guildId = ((ulong?)command.GuildId);
		ulong? userId = command.User.Id;
		await Task.Run(() =>
		{
			try
			{
				if (_boards.ContainsKey(userId))
				{

				}
			}
			catch (Exception err)
			{
				Console.WriteLine(err.Message);
			}
		}); */
	}
	public async Task ShowBoard(SocketSlashCommand command)
	{
		// do show board
		await command.RespondAsync("TODO: Show board");
	}
	public async Task SetPlaylist(SocketSlashCommand command)
	{
		await command.RespondAsync("TODO: Set playlist");
	}
	public async Task SetCycle(SocketSlashCommand command)
	{
		await command.RespondAsync("TODO: Set Cycle");
	}
	public async Task AddGuildToBoards(SocketGuild guild)
	{
		// Add guild to boards
	}
}
public enum Playlist
{
	Casual, Solo, Double, Standard, Best, Average
}
public enum Cycle
{
	Hourly, Daily, Weekly, Monthly
}
public struct Board
{
	ulong? BoardId { get; }
	Playlist SetPlaylist { get; set; }
	Cycle SetCycle { get; set; }
	List<Player> Players;
	Board(ulong? boardId)
	{
		Players = new List<Player>();
		BoardId = boardId;
		SetCycle = Cycle.Daily;
		SetPlaylist = Playlist.Average;
	}
}
public struct Player
{
	ulong? PlayerId { get; }
	string EpicTag { get; }
	int RankSolo
	{
		get
		{
			return FakeScore();
		}
	}
	int RankDouble
	{
		get
		{
			return FakeScore();
		}
	}
	int RankStandard
	{
		get
		{
			return FakeScore();
		}
	}
	int RankCasual
	{
		get
		{
			return FakeScore();
		}
	}
	int RankBest
	{
		get
		{
			return Math.Max(RankSolo, Math.Max(RankDouble, RankStandard));
		}
	}
	int RankAverage
	{
		get
		{
			return (RankSolo + RankDouble + RankStandard) / 3;
		}
	}
	int FakeScore()
	{
		Random rd = new Random();
		return rd.Next(500, 1500);
	}
	Player(ulong? playerId, string epicTag)
	{
		PlayerId = playerId;
		EpicTag = epicTag;
	}
}